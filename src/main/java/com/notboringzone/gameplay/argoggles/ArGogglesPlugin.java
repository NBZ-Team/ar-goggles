package com.notboringzone.gameplay.argoggles;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.notboringzone.gameplay.argoggles.listeners.PlayerJoin;
import com.notboringzone.gameplay.argoggles.listeners.PlayerMove;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import java.util.List;


public final class ArGogglesPlugin extends JavaPlugin {
    private ArmorStand armorStand;
    private Runnable script;

    @Override
    public void onEnable() {
        Injector injector = Guice.createInjector();
        injector.injectMembers(this);
        PlayerMove playerMove = injector.getInstance(PlayerMove.class);
        getServer().getPluginManager().registerEvents(playerMove, this);
        getServer().getPluginManager().registerEvents(injector.getInstance(PlayerJoin.class), this);

        for (Entity entity : getServer().getWorld("world").getEntities()) {
            if (entity instanceof ArmorStand) {
                armorStand = ((ArmorStand) entity);
                playerMove.setArmorStand(armorStand);
                armorStand.setHelmet(new ItemStack(Material.STONE_SWORD, 1, (short) 2));
                armorStand.setGravity(false);
                armorStand.setVisible(false);
                armorStand.setBodyPose(new EulerAngle(0, 0 , 0));
                armorStand.setHeadPose(new EulerAngle(0, 0 , 0));
            }
        }
        
        script = () -> {
            List<Player> list = ((List<Player>) getServer().getOnlinePlayers());
            if (list.isEmpty()) {
                runTaskLater();
                return;
            }
            Player player = list.get(0);
            Location location = player.getLocation();
            float rotation = (float) Math.toRadians(location.getYaw() + 90);
            Vector pos = new Vector(Math.cos(rotation), 0, Math.sin(rotation));
            pos.multiply(4);
            armorStand.teleport(location.add(pos));
            runTaskLater();
        };
        
        runTaskLater();
    }

    private void runTaskLater() {
        Bukkit.getScheduler().runTaskLater(this, script, 1);
    }
}
